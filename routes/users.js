var express = require("express");
var router = express.Router();
const path = require("path"),
  fs = require("fs-extra"),
  docusign = require("docusign-esign");

/* GET users listing. */
router.get("/auth/callback", async function (req, res, next) {
  const docusign = {
    accountID: "359ee861-48ed-49be-985b-5ab63fa9530f",
    username: "pyash451190@gmail.com",
    password: "patel@123",
    integratorKey: "2605308f-5f38-4ad7-baa0-4de4f4c3da4d",
  };

  const fileBytes = fs.readFileSync(
    path.resolve(__dirname, "sample-the-seeker-resume.pdf")
  ); // file

  const base64Doc = Buffer.from(fileBytes).toString("base64");

  const a = await axios({
    method: "post",
    url: `https://demo.docusign.net/restapi/v2.1/accounts/${docusign.accountID}/envelopes`,
    headers: {
      "X-DocuSign-Authentication": JSON.stringify({
        Username: docusign.username,
        Password: docusign.password,
        IntegratorKey: docusign.integratorKey,
      }),
      "Content-Type": "application/json",
    },
    data: {
      recipients: {
        signers: [
          {
            email: "yp35420@gmail.com",
            name: "Rutul",
            recipientId: 1,
            tabs: {
              signHereTabs: [
                {
                  xPosition: "320",
                  yPosition: "36",
                  documentId: "1",
                  pageNumber: "1",
                },
              ],
            },
          },
        ],
      },
      emailSubject: "DocuSign API - Signature Request on Document Call",
      documents: [
        {
          documentId: "1",
          name: "blank1.pdf",
          documentBase64: base64Doc,
        },
      ],
      status: "sent",
    },
  });
  console.log(a.data.envelopeId);
  res.json({
    message: "Sent Email!! Please Check your email and sign.",
  });
  try {
    const b = await axios({
      method: "post",
      url: `https://demo.docusign.net/restapi/v2.1/accounts/${docusign.accountID}/envelopes/${a.data.envelopeId}/views/recipient`,
      headers: {
        "X-DocuSign-Authentication": JSON.stringify({
          Username: docusign.username,
          Password: docusign.password,
          IntegratorKey: docusign.integratorKey,
        }),
        "Content-Type": "application/json",
      },
      data: {
        returnUrl: `https://demo.docusign.net/restapi/v2/accounts/${docusign.accountID}/envelopes`,
        authenticationMethod: "none",
        email: "yp35420@gmail.com",
        userName: "YP",
      },
    });
    console.log(b.data.url);
  } catch (error) {
    console.log(error);
  }
});

module.exports = router;
