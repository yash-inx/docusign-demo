var express = require("express");
var router = express.Router();
const axios = require("axios");
const path = require("path");
const fs = require("fs");
const { url } = require("inspector");
/* GET home page. */
router.get("/send", async function (req, res, next) {
  const docusign = {
    accountID: "359ee861-48ed-49be-985b-5ab63fa9530f",
    username: "pyash451190@gmail.com",
    password: "patel@123",
    integratorKey: "85abd0d9-091c-447f-80aa-07d2522bf225",
  };

  const fileBytes = fs.readFileSync(
    path.resolve(__dirname, "sample-the-seeker-resume.pdf")
  ); // file

  const base64Doc = Buffer.from(fileBytes).toString("base64");

  const a = await axios({
    method: "post",
    url: `https://demo.docusign.net/restapi/v2.1/accounts/${docusign.accountID}/envelopes`,
    headers: {
      "X-DocuSign-Authentication": JSON.stringify({
        Username: docusign.username,
        Password: docusign.password,
        IntegratorKey: docusign.integratorKey,
      }),
      "Content-Type": "application/json",
    },
    data: {
      recipients: {
        signers: [
          {
            email: "yp35420@gmail.com",
            name: "Rutul",
            recipientId: 1,
            clientUserId: "1001",
            tabs: {
              signHereTabs: [
                {
                  xPosition: "320",
                  yPosition: "50",
                  documentId: "1",
                  pageNumber: "1",
                },
              ],
            },
          },
        ],
      },
      emailSubject: "DocuSign API - Signature Request on Document Call",
      documents: [
        {
          documentId: "1",
          name: "blank1.pdf",
          documentBase64: base64Doc,
        },
      ],
      status: "sent",
    },
  });
  // console.log(a.data.envelopeId);

  try {
    const c = await axios({
      method: "post",
      url: `https://demo.docusign.net/restapi/v2.1/accounts/${docusign.accountID}/envelopes/${a.data.envelopeId}/views/recipient`,
      headers: {
        "X-DocuSign-Authentication": JSON.stringify({
          Username: docusign.username,
          Password: docusign.password,
          IntegratorKey: docusign.integratorKey,
        }),
        "Content-Type": "application/json",
      },
      data: {
        returnUrl: `https://www.docusign.com/`,
        authenticationMethod: "none",
        email: "yp35420@gmail.com",
        userName: "Rutul",
        recipientId: 1,
        clientUserId: "1001",
      },
    });
    res.json({
      message: "Sent Email!! Please Check your email and sign.",
      url: c.data.url,
    });
    console.log(c);
  } catch (error) {
    console.log(error);
  }
});

module.exports = router;
